package ru.my.SpringCourse.Scope;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Qualifier
public class TestSpring {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContextScope.xml");
        MusicPlayer computer1 = context.getBean("musicPlayer", MusicPlayer.class);
        MusicPlayer computer2 = context.getBean("musicPlayer", MusicPlayer.class);
        System.out.println(computer1 == computer2);
        context.close();
    }
}
