package ru.my.SpringCourse.Scope;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class MusicPlayer {

    private String name;
    private int volume;
    private Music music;

    @Autowired
    public MusicPlayer(@Qualifier("rockMusic") Music music, @Value("Sony") String name,
                       @Value("10") int volume) {
        this.music = music;
        this.name = name;
        this.volume = volume;
    }

    public String playMusic() {
        return "Playing: " + music.getSong();
    }

    @Override
    public String toString() {
        return "MusicPlayer{" +
                "name='" + name + '\'' +
                ", volume=" + volume +
                '}';
    }
}
