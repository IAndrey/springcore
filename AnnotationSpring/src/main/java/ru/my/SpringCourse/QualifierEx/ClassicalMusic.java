package ru.my.SpringCourse.QualifierEx;

import org.springframework.stereotype.Component;

@Component
public class ClassicalMusic implements Music {

    private String[] musics;

    public ClassicalMusic() {
        musics = new String[3];
        musics[0] = "Hungarian Rhapsody1";
        musics[1] = "Hungarian Rhapsody2";
        musics[2] = "Hungarian Rhapsody3";
    }

    @Override
    public String[] getSong() {
        return musics;
    }
}
