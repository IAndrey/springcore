package ru.my.SpringCourse.QualifierEx;

import org.springframework.stereotype.Component;

@Component
public class RockMusic implements Music {

    private String[] musics;

    public RockMusic() {
        musics = new String[3];
        musics[0] = "Wind cries Mary1";
        musics[1] = "Wind cries Mary2";
        musics[2] = "Wind cries Mary3";
    }

    @Override
    public String[] getSong() {
        return musics;
    }
}
