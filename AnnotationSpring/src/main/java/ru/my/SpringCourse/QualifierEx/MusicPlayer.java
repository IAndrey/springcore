package ru.my.SpringCourse.QualifierEx;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class MusicPlayer {

    private Music rockMusic;
    private Music classicalMusic;

    @Autowired
    public MusicPlayer(@Qualifier("rockMusic") Music rockMusic, @Qualifier("classicalMusic") Music classicalMusic) {
        this.rockMusic = rockMusic;
        this.classicalMusic = classicalMusic;
    }

    public String playMusic(MusicPlayerConstants.Genre genre) {
        Random random = new Random();
        int x = random.nextInt(3);
        if ("CLASSICAL".equals(genre.name())) {
            return "Playing: " + classicalMusic.getSong()[x];
        } else {
            return "Playing: " + rockMusic.getSong()[x];
        }
    }
}
