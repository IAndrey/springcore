package ru.my.SpringCourse.Value;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Qualifier
public class TestSpring {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContextValue.xml");
        MusicPlayer computer = context.getBean("musicPlayer", MusicPlayer.class);
        System.out.println(computer.toString());
        context.close();
    }
}
