package ru.my.SpringCourse.Value;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MusicPlayer {

    private String name;
    private int volume;
    private Music music;

    @Autowired
    public MusicPlayer(@Qualifier("rockMusic") Music music, @Value("${kvaaaa.name}") String name,
                       @Value("${kvaaaa.volume}") int volume) {
        this.music = music;
        this.name = name;
        this.volume = volume;
    }

    @PostConstruct
    private void init(){
        System.out.println("Create object MusicPlayer " + name);
    }

    @PreDestroy
    private void destroy(){
        System.out.println("Destroy object MusicPlayer " + name);
    }

    public String playMusic() {
        return "Playing: " + music.getSong();
    }

    @Override
    public String toString() {
        return "MusicPlayer{" +
                "name='" + name + '\'' +
                ", volume=" + volume +
                '}';
    }
}
