package ru.my.SpringCourse.Default;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MusicPlayer {

    //  Внедряем через конструктор
//  @Autowired
    private ClassicalMusic classicalMusic;
    private RockMusic rockMusic;
    //  @Value("Sony") - внедряем через сеттер
    private String name;

    @Autowired
    public MusicPlayer(ClassicalMusic classicalMusic, RockMusic rockMusic) {
        this.classicalMusic = classicalMusic;
        this.rockMusic = rockMusic;
    }

    public String playMusic() {
        return "Playing: " + classicalMusic.getSong();
    }

    public String getName() {
        return name;
    }

    @Value("Sony")
    public void setName(String name) {
        this.name = name;
    }
}
