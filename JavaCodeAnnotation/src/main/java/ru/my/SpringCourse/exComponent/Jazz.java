package ru.my.SpringCourse.exComponent;

import org.springframework.stereotype.Component;

@Component
public class Jazz implements Music {

    @Override
    public String getSong() {
        return "Jazz music";
    }
}
