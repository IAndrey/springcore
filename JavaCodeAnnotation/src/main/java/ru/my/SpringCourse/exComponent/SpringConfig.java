package ru.my.SpringCourse.exComponent;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.my.SpringCourse.exComponent")
public class SpringConfig {
}
