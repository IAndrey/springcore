package ru.my.SpringCourse.exComponent;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MusicPlayer {

    private String name;
    private int volume;
    private List<Music> music;

    @Autowired
    public MusicPlayer(List<Music> music,  @Value("Sony") String name, @Value("10") int volume) {
        this.music = music;
        this.name = name;
        this.volume = volume;
    }

    public String getName() {
        return name;
    }

    public int getVolume() {
        return volume;
    }

    @PostConstruct
    private void init() {
        System.out.println("Create object MusicPlayer " + name);
    }

    @PreDestroy
    private void destroy() {
        System.out.println("Destroy object MusicPlayer " + name);
    }

    public void playMusic() {
        music.forEach(music1 -> System.out.println(music1.getSong()));
    }

    @Override
    public String toString() {
        return "MusicPlayer{" +
                "name='" + name + '\'' +
                ", volume=" + volume +
                '}';
    }
}
