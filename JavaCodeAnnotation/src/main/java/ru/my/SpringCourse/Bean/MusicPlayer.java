package ru.my.SpringCourse.Bean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class MusicPlayer {

    private String name;
    private int volume;
    private Music music;

    public MusicPlayer(Music music, String name, int volume) {
        this.music = music;
        this.name = name;
        this.volume = volume;
    }

    public String getName() {
        return name;
    }

    public int getVolume() {
        return volume;
    }

    @PostConstruct
    private void init(){
        System.out.println("Create object MusicPlayer " + name);
    }

    @PreDestroy
    private void destroy(){
        System.out.println("Destroy object MusicPlayer " + name);
    }

    public String playMusic() {
        return "Playing: " + music.getSong();
    }

    @Override
    public String toString() {
        return "MusicPlayer{" +
                "name='" + name + '\'' +
                ", volume=" + volume +
                '}';
    }
}
