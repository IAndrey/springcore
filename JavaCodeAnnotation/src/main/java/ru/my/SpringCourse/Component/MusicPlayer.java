package ru.my.SpringCourse.Component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MusicPlayer {

    private String name;
    private int volume;
    private Music music;

    @Autowired
    public MusicPlayer(@Qualifier("classicalMusic") Music music, @Value("${kvaaaa.name}") String name,
                       @Value("${kvaaaa.volume}") int volume) {
        this.music = music;
        this.name = name;
        this.volume = volume;
    }


    public String playMusic() {
        return "Playing: " + music.getSong();
    }

    @Override
    public String toString() {
        return "MusicPlayer{" +
                "name='" + name + '\'' +
                ", volume=" + volume +
                '}';
    }
}
