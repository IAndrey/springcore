package ru.my.SpringCourse.Component;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("ru.my.SpringCourse.Component")
@PropertySource("classpath:musicPlayer.properties")
public class SpringConfig {

}
