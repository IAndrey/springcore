package ru.my.SpringCourse.exBean;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.my.SpringCourse.exComponent")
public class SpringConfig {

    @Bean
    public List<Music> musics(){
        List<Music> musics = new ArrayList<>();
        musics.add(new Jazz());
        musics.add(new ClassicalMusic());
        musics.add(new RockMusic());
        return musics;
    }

    @Bean
    public MusicPlayer musicPlayer(){
        return new MusicPlayer(musics(), "Sony", 10);
    }
}
