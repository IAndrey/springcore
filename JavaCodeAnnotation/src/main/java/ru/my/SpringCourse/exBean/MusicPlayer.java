package ru.my.SpringCourse.exBean;

import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class MusicPlayer {

    private String name;
    private int volume;
    private List<Music> music;

    public MusicPlayer(List<Music> music, String name, int volume) {
        this.music = music;
        this.name = name;
        this.volume = volume;
    }

    public String getName() {
        return name;
    }

    public int getVolume() {
        return volume;
    }

    @PostConstruct
    private void init() {
        System.out.println("Create object MusicPlayer " + name);
    }

    @PreDestroy
    private void destroy() {
        System.out.println("Destroy object MusicPlayer " + name);
    }

    public void playMusic() {
        Random random = new Random();
//        System.out.println(music.get(random.nextInt(2)).getSong());
        music.forEach(music1 -> System.out.println(music1.getSong()));
    }

    @Override
    public String toString() {
        return "MusicPlayer{" +
                "name='" + name + '\'' +
                ", volume=" + volume +
                '}';
    }
}
