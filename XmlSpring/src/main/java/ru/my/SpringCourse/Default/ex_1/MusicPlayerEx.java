package ru.my.SpringCourse.Default.ex_1;

import java.util.List;

import ru.my.SpringCourse.Default.Music;

public class MusicPlayerEx {

    private List<Music> musics;

    public MusicPlayerEx(List<Music> musics) {
        this.musics = musics;
    }

    public List<Music> getMusics() {
        return musics;
    }
}
