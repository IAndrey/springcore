package ru.my.SpringCourse.Default;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import ru.my.SpringCourse.Default.ex_1.MusicPlayerEx;

public class TestSpring {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContextSingleton.xml");
//        MusicPlayer music = context.getBean("musicPlayer", MusicPlayer.class);
//        System.out.println(music.getName());
//        System.out.println(music.getName2());
//        System.out.println(music.getVolume());
//        music.playMusic();
//        context.close();

//      Задание
        MusicPlayerEx player = context.getBean("exMusicPlayer", MusicPlayerEx.class);
        player.getMusics().forEach(music -> System.out.println(music.getSong()));
}
}
