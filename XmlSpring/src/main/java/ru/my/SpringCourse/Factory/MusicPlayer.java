package ru.my.SpringCourse.Factory;

public class MusicPlayer {

    private Music music;
    private String name;

    //IoC
    private MusicPlayer(Music music, String name) {
        this.music = music;
        this.name = name;
    }

    public static MusicPlayer factoryMusicPlayer(Music music, String name) {
        name += " (create with factory method).";
        return new MusicPlayer(music, name);
    }

    public String getName() {
        return name;
    }

    public void playMusic() {
        System.out.println("Playing: " + music.getSong());
    }

    public void doMyInit() {
        System.out.println("Initialization object: " + name);
    }

    public void doMyDestroy() {
        System.out.println("Destroy object: " + name);
    }
}
