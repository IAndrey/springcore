package ru.my.SpringCourse.Factory;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {

    public static void main(String[] args) {
//        MusicPlayer player = MusicPlayer.factoryMusicPlayer(new ClassicalMusic(), "Sony");
//        System.out.println(player.getName());
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContextFactory.xml");
        MusicPlayer music1 = context.getBean("musicPlayer", MusicPlayer.class);
        MusicPlayer music2 = context.getBean("musicPlayer", MusicPlayer.class);
        System.out.println(music1 == music2);
        music1.playMusic();
        music2.playMusic();
        context.close();
    }
}
