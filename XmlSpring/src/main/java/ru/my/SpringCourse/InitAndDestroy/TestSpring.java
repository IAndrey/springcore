package ru.my.SpringCourse.InitAndDestroy;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContextInitAndDestroy.xml");
        MusicPlayer music1 = context.getBean("musicPlayerScope", MusicPlayer.class);
        music1.playMusic();
        context.close();
    }
}
