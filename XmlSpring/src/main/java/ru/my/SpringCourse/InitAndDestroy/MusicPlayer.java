package ru.my.SpringCourse.InitAndDestroy;

public class MusicPlayer {

    private Music music;
    private String name;
    private int volume;

    //IoC
    public MusicPlayer(Music music, String name) {
        this.music = music;
        this.name = name;
    }

    public void setMusic(Music music) {
        this.music = music;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public void playMusic() {
        System.out.println("Playing: " + music.getSong());
    }

    public void doMyInit(){
        System.out.println("Initialization object: " + name);
    }
    public void doMyDestroy() {
        System.out.println("Destroy object: " + name);
    }

}
