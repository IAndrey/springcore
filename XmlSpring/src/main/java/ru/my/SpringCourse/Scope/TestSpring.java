package ru.my.SpringCourse.Scope;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContextScope.xml");
        MusicPlayer music1 = context.getBean("musicPlayerScope", MusicPlayer.class);
        MusicPlayer music2 = context.getBean("musicPlayerScope", MusicPlayer.class);
        System.out.println(music1 == music2);
    }
}
